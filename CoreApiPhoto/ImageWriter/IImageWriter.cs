﻿using Microsoft.AspNetCore.Http;

using System.Threading.Tasks;

namespace CoreApiPhoto.ImageWriter
{
    public interface IImageWriter
    {
        Task<string> UploadImage(IFormFile file);
    }
}
