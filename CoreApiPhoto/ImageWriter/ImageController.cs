﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

/*
    https://www.codeproject.com/Articles/1256591/Upload-Image-to-NET-Core-2-1-API

    POSTMAN
    https://localhost:xxxxx/api/image
    amb "POST"
    indicant a body una clau "file" de tipus "file" que apunti a la imatge q volem enviar

    el projecte ha de tenir carpetes /wwwroot/images
    per després veure la imatge és directament a /images (sense api)
    les hi assigna un nom aleatori, que és la resposta de la API

    https://localhost:44354/images/206d394e-659c-45d0-9bc0-c4caf0be082f.jpg

*/

namespace CoreApiPhoto.ImageWriter
{
    [Route("api/image")]
    public class ImagesController : Controller
    {
        private readonly IImageHandler _imageHandler;

        public ImagesController(IImageHandler imageHandler)
        {
            _imageHandler = imageHandler;
        }

        /// <summary>
        /// Uplaods an image to the server.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            return await _imageHandler.UploadImage(file);
        }
    }
}
